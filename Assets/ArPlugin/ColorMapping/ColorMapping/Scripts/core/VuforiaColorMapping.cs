﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using UnityEngine;
using UnityEngine.UI;

using Vuforia;

public class VuforiaColorMapping : MonoBehaviour
{
    public ImageTargetBehaviour imageTarget;

    public GameObject arContents;

    public Renderer RenderColoringItem;

    public int realWidth;
    public int realHeight;

    private GameObject drawObj;
    private GameObject cube;

    private float counter = 0;

    void Start()
    {
        Vector2 imageTargetSize = imageTarget.GetSize();
        float targetWidth = imageTargetSize.x;
        float targetHeight = imageTargetSize.y;

        cube = CreateCubeForVuforiaTarget(this.gameObject, targetWidth, targetHeight);
    }

    private void FixedUpdate()
    {
        if (counter > 0.3f)
        {
            counter = 0;
            this.Play();
        }

        counter += Time.fixedDeltaTime;
    }

    public void Play()
    {
        if (this.imageTarget.CurrentStatus == TrackableBehaviour.Status.TRACKED)
        {
            float[] srcValue = AirarManager.Instance.CalculateMarkerImageVertex(cube);
            var texture = ScreenShot.GetScreenShot(arContents);

            AirarManager.Instance.ProcessColoredMapTexture(texture, srcValue, realWidth, realHeight, (resultTex) =>
            {
                this.RenderColoringItem.material.SetTexture("_BaseMap", resultTex);
            });
        }
    }

    /// <summary>
    /// Create a full size cube on the Vuforia marker image
    /// </summary>
    /// <param name="targetWidth">marker image width</param>
    /// <param name="targetHeight">marker image height</param>
    public GameObject CreateCubeForVuforiaTarget(GameObject parentObj, float targetWidth, float targetHeight)
    {
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.GetComponent<Renderer>().material = AirarManager.Instance.transparentMat;
        cube.transform.SetParent(parentObj.transform);
        cube.transform.localPosition = Vector3.zero;
        cube.transform.localScale = new Vector3(targetWidth, 0.001f, targetHeight);

        return cube;
    }
}
