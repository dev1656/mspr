﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System.IO;
using UnityEngine.Networking;
using System;

public class AirarManager : AirarSingleton<AirarManager>
{
    public Material transparentMat;
    private Texture2D croppedTexture;

#if UNITY_ANDROID
    [DllImport("AirarColorMap")]
#elif UNITY_IOS
    [DllImport("__Internal")]
#endif
    private static extern void ImageProc(string imgPath, float[] src, int height, int width);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="srcTexture">screenshot</param>
    /// <param name="src">float[8]</param>
    /// <param name="height">target 3d model map height size</param>
    /// <param name="width">target 3d model map width size</param>
    public void ProcessColoredMapTexture(Texture2D srcTexture ,float[] src,int height, int width,Action<Texture2D> callback)
    {
        var widthCrop = src[2] - src[0];
        var heightCrop = src[1] - src[7];

        if (croppedTexture != null)
        {
            GameObject.Destroy(croppedTexture);
        }

        if (src[6] > 0 && src[7] > 0)
        {
            croppedTexture = new Texture2D((int)widthCrop, (int)heightCrop);
            Color[] pixels = srcTexture.GetPixels((int)(src[6]), (int)(src[7]), (int)widthCrop, (int)heightCrop);
            croppedTexture.SetPixels(pixels);
            croppedTexture.Apply();
            callback(croppedTexture);
        }

        //if (croppedTexture != null)
        //{
        //    GameObject.Destroy(croppedTexture);
        //}
    }

    /// <summary>
    /// Marker image vertex coordinate calculation
    /// </summary>
    /// <param name="cube">cube object on marker image</param>
    public float[] CalculateMarkerImageVertex(GameObject cube)
    {
        List<Vector2> vertexList = new List<Vector2>();
        float[] srcPosition = new float[8];
        Vector3[] vertices = cube.GetComponent<MeshFilter>().mesh.vertices;
        Vector2[] result = new Vector2[vertices.Length];
        for (int i = 0; i < vertices.Length; ++i)
        {
            result[i] = Camera.main.WorldToScreenPoint(cube.transform.TransformPoint(vertices[i]));
            vertexList.Add(result[i]);
        }

        for (int i = 12; i < 16; i++)
        {
            switch (i)
            {
                case 12:
                    srcPosition[4] = vertexList[12].x;
                    srcPosition[5] = vertexList[12].y;
                    break;
                case 13:
                    srcPosition[2] = vertexList[13].x;
                    srcPosition[3] = vertexList[13].y;
                    break;
                case 14:
                    srcPosition[0] = vertexList[14].x;
                    srcPosition[1] = vertexList[14].y;
                    break;
                case 15:
                    srcPosition[6] = vertexList[15].x;
                    srcPosition[7] = vertexList[15].y;
                    break;

                default:
                    break;
            }
        }

        return srcPosition;
    }

}
