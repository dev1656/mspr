﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public static class ScreenShot
{
    private static Rect rect;
    private static RenderTexture renderTexture;
    private static Texture2D screenShot;

    public static Texture2D GetScreenShot(GameObject arContents)
    {
        arContents.SetActive(false);

        if (Camera.main.targetTexture != null)
        {
            Camera.main.targetTexture.Release();
        }

        if (renderTexture != null)
        {
            renderTexture.Release();
        }

        if (screenShot != null)
        {
            GameObject.Destroy(screenShot);
        }

        int sceenWidth = Screen.width;
        int sceenHeight = Screen.height;

        rect = new Rect(0, 0, sceenWidth, sceenHeight);
        renderTexture = new RenderTexture(sceenWidth, sceenHeight, 24);
        screenShot = new Texture2D(sceenWidth, sceenHeight, TextureFormat.RGB24, false);

        Camera.main.targetTexture = renderTexture;
        Camera.main.Render();

        RenderTexture.active = renderTexture;
        screenShot.ReadPixels(rect, 0, 0);
        screenShot.Apply();

        Camera.main.targetTexture = null;
        RenderTexture.active = null;

        arContents.SetActive(true);

        return screenShot;
    }
}