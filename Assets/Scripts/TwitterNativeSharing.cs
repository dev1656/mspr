﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TwitterNativeSharing: MonoBehaviour
{

	public Button shareButton;

	private bool isFocus = false;

	private string shareText = "#cerealis #coloring #AR";

	void Start()
	{
		shareButton.onClick.AddListener(OnShareButtonClick);
	}

	void OnApplicationFocus(bool focus)
	{
		isFocus = focus;
	}

	public void OnShareButtonClick()
	{
		StartCoroutine(ShareScreenshot());
	}


	public IEnumerator ShareScreenshot()
	{
		string screenShotPath = CRM.GetScreenshotPath();

		//Create intent for action send
		AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));

		AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaObject unityContext = currentActivity.Call<AndroidJavaObject>("getApplicationContext");
		string packageName = unityContext.Call<string>("getPackageName");

		string authority = packageName + ".fileprovider";

		AndroidJavaObject fileObj = new AndroidJavaObject("java.io.File", screenShotPath);

		AndroidJavaClass fileProvider = new AndroidJavaClass("androidx.core.content.FileProvider");
		AndroidJavaObject uri = fileProvider.CallStatic<AndroidJavaObject>("getUriForFile", unityContext, authority, fileObj);

		//create image URI to add it to the intent
		AndroidJavaObject uriObject = fileProvider.CallStatic<AndroidJavaObject>("getUriForFile", unityContext, authority, fileObj);

		//put image and string extra
		int FLAG_GRANT_READ_URI_PERMISSION = intentClass.GetStatic<int>("FLAG_GRANT_READ_URI_PERMISSION");

		intentObject.Call<AndroidJavaObject>("setType", "image/*");
		intentObject.Call<AndroidJavaObject>("setPackage", "com.twitter.android");
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText);
		intentObject.Call<AndroidJavaObject>("addFlags", FLAG_GRANT_READ_URI_PERMISSION);

		AndroidJavaObject chooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, null);
		currentActivity.Call("startActivity", chooser);


		yield return new WaitUntil(() => isFocus);
	}
}

