﻿namespace Tests
{
    using System.Collections;
    using System.Collections.Generic;
    using NUnit.Framework;
    using UnityEngine;
    using UnityEngine.Networking;
    using UnityEngine.TestTools;

    public class CRMTests
    {
        public GameObject refCRM;
        public GameObject instanceCRM;
        public CRM CRM;

        [SetUp]
        public void Setup()
        {
            refCRM = Resources.Load<GameObject>("CRMManager");
            instanceCRM = MonoBehaviour.Instantiate(refCRM);
            CRM = instanceCRM.GetComponent<CRM>();

        }

        //[TearDown]
        //public void TearDown()
        //{
        //    MonoBehaviour.Destroy(refCRM);
        //}


        // A Test behaves as an ordinary method
        [UnityTest]
        public IEnumerator CRMTestSwitchToShareViewIsRequestDone()
        {
            CRM.IsRequestDone = true;
            CRM.SwitchToShareView();
            yield return null;

            Assert.IsTrue(CRM.SharePanel.activeSelf);
            Assert.IsFalse(CRM.CRMView.activeSelf);
            Assert.IsTrue(CRM.SocialNetworkView.activeSelf);
        }
        
        // A Test behaves as an ordinary method
        [UnityTest]
        public IEnumerator CRMTestSwitchToShareViewIsRequestNotDone()
        {
            CRM.IsRequestDone = false;
            CRM.SwitchToShareView();
            yield return null;

            Assert.IsTrue(CRM.SharePanel.activeSelf);
            Assert.IsTrue(CRM.CRMView.activeSelf);
            Assert.IsFalse(CRM.SocialNetworkView.activeSelf);
        }
    }
}
