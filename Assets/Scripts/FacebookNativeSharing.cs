﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Networking;

public class FacebookNativeSharing : MonoBehaviour
{
	public Button shareButton;

	private bool isFocus = false;

	private string shareText;

	void Start()
	{
		shareButton.onClick.AddListener(OnShareButtonClick);
	}

	void OnApplicationFocus(bool focus)
	{
		isFocus = focus;
	}

	public void OnShareButtonClick()
	{
		ShareScreenshot();
	}

	private void ShareScreenshot()
	{
		StartCoroutine(ShareScreenshotInAnroid());
	}


	public IEnumerator ShareScreenshotInAnroid()
	{
		WWWForm form = CRM.GetForm();
		
		string api_url = "http://164.132.228.51:3000/api/v1/upload";

		using (UnityWebRequest webRequest = UnityWebRequest.Post(api_url, form))
		{
			yield return webRequest.SendWebRequest();

			if (webRequest.responseCode == 201)
			{
				ImageFileName filename = JsonUtility.FromJson<ImageFileName>(webRequest.downloadHandler.text);
				shareText = "http://164.132.228.51:3000/api/v1/screenshot/" + filename.name;

				AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");

				AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

				AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
				AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

				intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
				intentObject.Call<AndroidJavaObject>("setType", "text/plain");
				intentObject.Call<AndroidJavaObject>("setPackage", "com.facebook.katana");
				
				intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText);

				currentActivity.Call("startActivity", intentObject);

				yield return new WaitUntil(() => isFocus);
			}
			else
			{
				Debug.Log("erreur");
				Debug.Log(webRequest.responseCode);
			}
		}
	}
}

