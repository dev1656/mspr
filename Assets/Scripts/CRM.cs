﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using CandyCoded.env;

public class CRM : MonoBehaviour
{
    public GameObject CRMView;

    public InputField EmailText;

    public InputField FirstnameText;

    public GameObject SocialNetworkView;

    public GameObject SharePanel;

    public GameObject FeedBackPanel;

    private string CRMID;


    public bool IsRequestDone = false;

    private static WWWForm form;
    private static string screenShotPath;

    private void Start()
    {
        if (env.TryParseEnvironmentVariable("CRMID", out string id))
        {
            CRMID = id;
        }
    }

    public void TakeScreenshotAndSwitchView()
    {
        StartCoroutine(TakeScreenshot());   
    }
    private IEnumerator TakeScreenshot()
    {
        yield return new WaitForEndOfFrame();
        string screenshotName = "mspr.png";
        screenShotPath = Application.persistentDataPath + "/" + screenshotName;
        ScreenCapture.CaptureScreenshot(screenshotName, 1);

        Texture2D texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        texture.Apply();
        form = new WWWForm();
        form.AddBinaryData("image", texture.EncodeToPNG());
        yield return new WaitForSeconds(0.5f);
        SwitchToShareView();
    }

    public static string GetScreenshotPath()
    {
        return screenShotPath;
    }

    public static WWWForm GetForm()
    {
        return form;
    }

    public void SendToCRM()
    {
        CRMData datas = new CRMData();
        CRMProperties properties = new CRMProperties();
        properties.email = EmailText.text;
        properties.firstname = FirstnameText.text;
        if (properties.email != "" && properties.firstname != "")
        {
            datas.properties = properties;
            StartCoroutine(UploadDatas(datas));
        }
        else
        {
            StartCoroutine(Popup("Please enter your email and firstname"));
        }
    }

    public void SwitchToShareView()
    {
        this.SharePanel.SetActive(true);
        this.CRMView.SetActive(false);
        this.SocialNetworkView.SetActive(false);

        if (this.IsRequestDone)
        {
            this.SocialNetworkView.SetActive(true);
        }
        else
        {
            this.CRMView.SetActive(true);
        }
    }

    IEnumerator UploadDatas(CRMData datas)
    {
        var jsonData = JsonUtility.ToJson(datas);
        string uri = "https://api.hubapi.com/crm/v3/objects/contacts?hapikey=" + CRMID;

        using (UnityWebRequest webRequest = UnityWebRequest.Put(uri, jsonData))
        {
            webRequest.method = "POST";
            webRequest.SetRequestHeader("accept", "application/json; charset=UTF-8");
            webRequest.SetRequestHeader("content-type", "application/json; charset=UTF-8");

            yield return webRequest.SendWebRequest();

            // 201 = création dans le crm, 409 = mail + firstname existent déjà dans le crm donc on laisse passer
            if (webRequest.responseCode == 201 || webRequest.responseCode == 409)
            {
                this.IsRequestDone = true;
                SwitchToShareView();
            }
            // 400 = bad request, mail / firstname incorrect
            else if (webRequest.responseCode == 400)
            {
                Debug.Log("erreur 400");
                StartCoroutine(Popup("Incorrect email format."));
            }
            else
            {
                Debug.Log(webRequest.responseCode);
                StartCoroutine(Popup("The servor encounters an error, please try again."));
            }
        }
    }


    IEnumerator Popup(string message)
    {
        FeedBackPanel.GetComponentInChildren<Text>().text = message;
        FeedBackPanel.SetActive(true);
        yield return new WaitForSeconds(5);
        FeedBackPanel.SetActive(false);
    }
}
